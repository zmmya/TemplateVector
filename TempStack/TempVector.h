#include <iostream>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <string.h>
using namespace std;

template<class T>

//vector是类名
//vector<T>是类型
class Vector
{
public:
  Vector();

  ~Vector();

  Vector(const Vector<T>& v);
  Vector<T>& operator=(const Vector<T>& v);

  size_t Size();
  size_t Capacity();

  void PushBack(const T& x);

  void PopBack();
  void Expand(size_t n);
  void Insert(size_t pos,const T& x);
  //删除指定位置的值
  void Erase(size_t pos);
  void Print();
  const T& Back() const;
  bool Empty();
protected:
  T *_start;
  T *_finish;
  T *_endofstorage;
};


#include "TempVector.cpp"
