
//vector是类名
//vector<T>是类型
template<class T>
Vector<T>::Vector()
  :_start(NULL),
   _finish(NULL),
   _endofstorage(NULL)
{}

template<class T>
Vector<T>::~Vector()
{
  delete _start;
  _start = _finish = _endofstorage = NULL;
}

//template<class T>
//Vector<T>::Vector(const Vector<T>& v);
//template<class T>
//Vector<T>::Vector<T>& operator=(const Vector<T>& v);

template<class T>
size_t Vector<T>::Size()
{
  return _finish-_start;
}
template<class T>
size_t Vector<T>::Capacity()
{
  return _endofstorage-_start;
}

template<class T>
void Vector<T>::PushBack(const T& x)
{
  Insert(Size(),x);
}

template<class T>
void Vector<T>::PopBack()
{
  if(Size() == 0)
  {
    return;
  }
  --_finish;
  return;
}
template<class T>
void Vector<T>::Expand(size_t n)
{
  if(Size() == 0)
  {
    _start = new T[3];
    _finish = _start;
    _endofstorage = _start + n;
  }
  T* new_n = new T[n];
  size_t size = Size();
  memcpy(new_n,_start,sizeof(T) * size);
  delete []_start;

  _start = new_n;
  _finish = _start + size;
  _endofstorage = _start + n;
}
template<class T>
void Vector<T>::Insert(size_t pos,const T& x)
{
  assert(pos <= Size());
  if(Size() == Capacity())
  {
    Expand(Capacity() * 2);
  }
  T* end = _finish-1;
  while(end >= _start + pos)
  {
    *(end + 1) = *end;
    --end;
  }
  _start[pos] = x;
  ++_finish;
}
//删除指定位置的值
template<class T>
void Vector<T>::Erase(size_t pos)
{
  if(pos > Size())
  {
    return;
  }
  //从pos位置的下一个位置起
  T *cur = _start + pos; 
  while(cur != _finish)
  {
    *cur = *(cur + 1);
    cur++;
  }
  --_finish;
  return;
}
template<class T>
void Vector<T>::Print()
{
  T *cur = _start;
  while(cur != _finish)
  {
    cout<<*cur<<" ";
    ++cur;
  }
  cout<<endl;
}

template <class T>
bool Vector<T>::Empty()
{
  if(_start == _finish)
  {
    return true;
  }
  return false;
}
template <class T>
const T& Vector<T>::Back() const 
{
  return *(_finish-1);
}

//template <class T>

//int main()
//{
//  Vector<int> v;
//  v.PushBack(1);
//  v.PushBack(2);
//  v.PushBack(3);
//  v.PushBack(4);
//  v.PopBack();
//  v.Erase(1);
//  v.Print();
//}
