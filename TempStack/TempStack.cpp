#include<stdio.h>
#include<iostream>
#include<vector>
#include"TempVector.h"
using namespace std;
//#pragma once

//模板类的模板

//Container:容器
//Stack:适配器
template <class T,class Container>
class Stack{
public:
  void Push(const T& x){
    _con.PushBack(x);
  }
  void Pop(){
    _con.PopBack();
  }
  //T& Top(){
  //  return _con.Back();
  //}
  const T& Top() const{
    return _con.Back();
  }
  size_t Size(){
    return _con.Size();
  }
  bool Empty(){
    return _con.Empty();
  }
protected:
  Container _con;
};


int main()
{
  Stack<int,Vector<int> > s;

  s.Push(1);
  s.Push(2);
  s.Push(3);
  s.Push(4);

  while(!s.Empty())
  {
    cout<<s.Top()<<" ";
    s.Pop();
  }
  cout<<endl;

  return 0;
}
