#include <iostream>
using namespace std;

template <class T>

void swap(T *a,T *b)
{
  T tmp = *a;
  *a = *b;
  *b = tmp;
}

void swap(int *a,int *b)
{
  int tmp = *a;
  *a = *b;
  *b = tmp;
}
void swap(char *a,char *b)
{
  char tmp = *a;
  *a = *b;
  *b = tmp;
}
int main()
{
  int a = 1;
  int b = 2;
  swap(a,b);
  cout<<a <<b <<endl;

  char c = 'c';
  char d = 'd';
  swap(c,d);
  cout<<c <<d <<endl;
  return 0;
}


//template <class T1,class T2>
//bool IsEqual(const T1& left,const T2& right)
//{
//  return left == right;
//}
//
//template<class T>
//bool IsEqual(const T& left,const T& right)
//{
//  return left == right;
//}
//
//int main()
//{
//  cout<<IsEqual<int>(1,1)<<endl;
//
//  cout<<IsEqual<int,float>(1,1.2)<<endl;
//  return 0;
//}



