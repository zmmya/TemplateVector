#include"TempQueue.h"

int main()
{
  Queue<int,List<int> > q;
  q.Push(1);
  q.Push(2);
  q.Push(3);
  q.Push(4);

  while(!q.Empty())
  {
    cout<<q.Front()<<" ";
    q.Pop();
  }
  cout<<endl;
  return 0;
}
