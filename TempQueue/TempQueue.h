#pragma once

#include<stdio.h>
#include<iostream>
#include"../TempletList/TempletList.h"
using namespace std;

//模板类的模板参数
template <class T, class Container = List<T> >
class Queue
{
public:
  //往队尾插入节点
  void Push(const T& x)
  {
    _con.PushBack(x);
  }

  //删除队首节点
  void Pop()
  {
    _con.Pop();
  }
  //取队首节点
  const T& Front()
  {
    return _con.Front();
  }

  bool Empty() 
  {
    return _con.Empty();
  }
  size_t Size() const
  {
    return _con.Size();
  }

protected:
    Container _con;
};


